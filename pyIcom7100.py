#!/usr/bin/python3
#Icom 7100 Control 
#LU9CBL - Matias Graino

import serial
import codecs
import yaml
#https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/


#Global Variables
com = "COM3"
baudrate = 19200
ser = serial.Serial(com, baudrate) #Define COM and baudrate of serial
preamble = "fe"
trxadd = "88"   #CIV Address for the Icom equipment
ctrladd = "E0"
trailer = "fd"

def main():

    url_file = "config.yaml"
    load_config_file(url_file)


    # frecuencia = get_frequency(ser,preamble,trxadd,ctrladd,trailer)
    # print(frecuencia)

    # modo_operacion = get_operating_mode(ser,preamble,trxadd,ctrladd,trailer)
    # print(modo_operacion)

    # mode = 'FM'
    # set_operating_mode(ser,preamble,trxadd,ctrladd,trailer, mode)

    # frequency = 147857920 #Hz
    # set_frequency(ser,preamble,trxadd,ctrladd,trailer,frequency)

    #VFO-A / VFO-B / VFO-A-EQUAL-B / VFO-A-XCHANGE-B
    # task = "VFO-A-EQUAL-B"
    # select_vfo(ser,preamble,trxadd,ctrladd,trailer, task)

    #ON/OFF Equipment
    #on_off = "ON"
    #on_off_equipment(ser,preamble,trxadd,ctrladd,trailer,on_off, baudrate)

    #ON/OFF Tone Squelch TSQL
    #on_off = "OFF"
    #on_off_tone_squelch(ser,preamble,trxadd,ctrladd,trailer, on_off)

    #ON/OFF Repeater Tone
    #on_off = "OFF"
    #on_off_repeater_tone(ser,preamble,trxadd,ctrladd,trailer, on_off)

    #READ / SET Repeater Tone
    #rpt_tone = read_repeater_tone(ser,preamble,trxadd,ctrladd,trailer)
    #print(rpt_tone)
    
    #ON/OFF Split Function 
    #on_off = "OFF"
    #on_off_split(ser,preamble,trxadd,ctrladd,trailer, on_off)

    #rpt_tone = 123.0 #123.0 / 114.8 / 100.0 / 94.8 / etc.
    #set_repeater_tone(ser,preamble,trxadd,ctrladd,trailer,rpt_tone)

    #Memory Operation
    #memory_bank = "A"
    #select_memory_bank(ser,preamble,trxadd,ctrladd,trailer,memory_bank)
    
    #channel = 2
    #select_memory_channel(ser,preamble,trxadd,ctrladd,trailer,channel)

    #S-Meter Level
    #smeter = read_smeter_level(ser,preamble,trxadd,ctrladd,trailer)
    #print(smeter)

    


    ########################################
    # TESTING!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ########################################

    #Memory Channel Content
    #memory_content = get_memory_content(ser,preamble,trxadd,ctrladd,trailer)
    #print(memory_content)

    #TESTTTTTT
    #power_level = get_power_level_meter(ser,preamble,trxadd,ctrladd,trailer)
    #print(power_level)

    #Close serial Communication
    ser.close()


def load_config_file(url_file):
    with open(url_file) as file:
        load_test = yaml.load(file, Loader=yaml.FullLoader)
    print(load_test)

def write_config_file(url_file):
    dict_file = [{'Memoria 1' : ['soccer', 'football', 'basketball', 'cricket', 'hockey', 'table tennis']},
{'countries' : ['Pakistan', 'USA', 'India', 'China', 'Germany', 'France', 'Spain']}]

    with open(r'E:\data\store_file.yaml', 'w') as file:
        documents = yaml.dump(dict_file, file)

#Frequency
def get_frequency(ser,preamble,trxadd,ctrladd,trailer):
    try:
        #ser = serial.Serial('COM3', 19200) 
        command = "03"
        #subcommand = "00"
        data = ""
        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)
        count = 0
        while count <= 1:
            for x in range(0, 5):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        #ser.close()
        resultado = info[8]+info[9]+info[6]+info[7]+"."+info[4]+info[5]+info[2]+"."+info[3]+info[0]+info[1]+"Hz"
        return resultado.lstrip("0")
    except Exception as e:
        print(e)
        ser.close()

def set_frequency(ser,preamble,trxadd,ctrladd,trailer,frequency):
    try:
        command = "00"
        #subcommand = "00"
        
        freq = str(frequency)
        data = freq[7]+freq[8]+freq[5]+freq[6]+freq[3]+freq[4]+freq[1]+freq[2]+'0'+freq[0]
        cmd_send = preamble + preamble + trxadd + ctrladd + command + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)
        
        
    except Exception as e:
        print(e)
        ser.close()

#Operating Mode (LSB, USB, FM, DV, etc.)
def get_operating_mode(ser,preamble,trxadd,ctrladd,trailer):
    try: 
        command = "04"
        #subcommand = "00"
        data = ""
        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

        count = 0
        while count <= 1:
            for x in range(0, 5):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        filter_setting = info[2:]
        filter_name = ''

        if filter_setting == '01':
            filter_name = 'FIL1'
        elif filter_setting == '02':
            filter_name = 'FIL2'
        elif filter_setting == '03':
            filter_name = 'FIL3'

        operating_mode = info[:2]
        if operating_mode == '00':
            mode = 'LSB'
        elif operating_mode == '01':
            mode = 'USB'
        elif operating_mode == '02':
            mode = 'AM'
        elif operating_mode == '03':
            mode = 'CW'
        elif operating_mode == '04':
            mode = 'RTTY'
        elif operating_mode == '05':
            mode = 'FM'
        elif operating_mode == '06':
            mode = 'WFM'
        elif operating_mode == '07':
            mode = 'CW-R'
        elif operating_mode == '08':
            mode = 'RTTY-R'
        elif operating_mode == '17':
            mode = 'DV'
        
        return mode,filter_name

    except Exception as e:
        print(e)
        ser.close()

def set_operating_mode(ser,preamble,trxadd,ctrladd,trailer, mode):
    try: 
        command = "06"
        #subcommand = "00"
        
        if mode == 'LSB':
            data = '00'
        elif mode == 'USB':
            data = '01'
        elif mode == 'AM':
            data = '02'
        elif mode == 'CW':
            data = '03'
        elif mode == 'RTTY':
            data = '04'
        elif mode == 'FM':
            data = '05'
        elif mode == 'WFM':
            data = '06'
        elif mode == 'CW-R':
            data = '07'
        elif mode == 'RTTY-R':
            data = '08'
        elif mode == 'DV':
            data = '17'

        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#VFO mode
def select_vfo(ser,preamble,trxadd,ctrladd,trailer, task):
    try: 
        command = "07"
        subcommand = ""
        
        if task == 'VFO-A':
            data = '00'
        elif task == 'VFO-B':
            data = '01'
        elif task == 'VFO-A-EQUAL-B':
            data = 'A0'
        elif task == 'VFO-A-XCHANGE-B':
            data = 'B0'
        
        cmd_send = preamble + preamble + trxadd + ctrladd + command + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#ON/OFF Equipment
def on_off_equipment(ser,preamble,trxadd,ctrladd,trailer,on_off, baudrate):
    try:
        command = "18"
        if on_off == "ON":
            subcommand = "01"
            preambleFixed = ""
            #The preambleFixed depends of the baudrate for the serial communication.
            #Baudrate 19200 bps -> 25 FEs
            #Baudrate 9600 bps -> 13 FEs
            #Baudrate 4800 bps -> 7 FEs
            #Baudrate 1200 bps -> 3 FEs
            #Baudrate 300 bps -> 2 FEs
            if baudrate == 19200:
                for num in range(25):
                    preambleFixed = preambleFixed + "fe" 
            cmd_send = preambleFixed + preamble + preamble + trxadd + ctrladd + command + subcommand + trailer

        elif on_off == "OFF":
            subcommand = "00"
            cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        else:
            raise Exception("Not Valid option, only ON or OFF")
        
        

        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

      
        
    except Exception as e:
        print(e)
        ser.close()

#ON/OFF Tone Squelch TSQL
def on_off_tone_squelch(ser,preamble,trxadd,ctrladd,trailer, on_off):
    try: 
        command = "16"
        subcommand = "43"        
        if on_off == 'ON':
            data = '01'
        elif on_off == 'OFF':
            data = '00'
        else:
            raise Exception("Not Valid option, only ON or OFF")
        
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#ON/OFF Repeater Tone 
def on_off_repeater_tone(ser,preamble,trxadd,ctrladd,trailer, on_off):
    try: 
        command = "16"
        subcommand = "42"        
        if on_off == 'ON':
            data = '01'
        elif on_off == 'OFF':
            data = '00'
        else:
            raise Exception("Not Valid option, only ON or OFF")
        
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#READ/SET Repeater Tone
def read_repeater_tone(ser,preamble,trxadd,ctrladd,trailer):
    try:
        command = "1b"
        subcommand = "00"
        data = ""
        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

        count = 0
        while count <= 1:
            for x in range(0, 6):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        resultado = info[0]+info[1]+info[2]+info[3]+info[4]+"."+info[5]+"Hz"
        return resultado.lstrip("0")

    except Exception as e:
        print(e)
        ser.close()

def set_repeater_tone(ser,preamble,trxadd,ctrladd,trailer,rpt_tone):
    try:
        command = "1b"
        subcommand = "00"
        print(str(rpt_tone))
        data = str(rpt_tone).replace(".","")
        print(data)
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#ON/OFF Split Function 
def on_off_split(ser,preamble,trxadd,ctrladd,trailer, on_off):
    try: 
        command = "0f"   
        if on_off == 'ON':
            data = '01'
        elif on_off == 'OFF':
            data = '00'
        else:
            raise Exception("Not Valid option, only ON or OFF")
        
        cmd_send = preamble + preamble + trxadd + ctrladd + command + data + trailer
        byte_array = bytearray.fromhex(cmd_send)
        ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

#Memory Operation
def select_memory_bank(ser,preamble,trxadd,ctrladd,trailer,memory_bank):
    try:
        command = "08"
        subcommand = "a0"
        
        if memory_bank == 'A':
            subcommand2 = '01'
        elif memory_bank == 'B':
            subcommand2 = '02'
        elif memory_bank == 'C':
            subcommand2 = '03'
        elif memory_bank == 'D':
            subcommand2 = '04'
        elif memory_bank == 'E':
            subcommand2 = '05'
        else:
            raise Exception("Not Valid option, only A, B, C, D and E")
        

        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + subcommand2 + trailer
        byte_array = bytearray.fromhex(cmd_send)
        ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()

def select_memory_channel(ser,preamble,trxadd,ctrladd,trailer,channel):
    try:
        command = "08"
        subcommand = "a0"
        
        if channel >= 1 and channel <= 9:
            subcommand = "0" + str(channel)
        elif channel >= 10 and channel <= 99:
            subcommand = str(channel)
        elif channel == 100:
            subcommand = '1a'
        elif channel == 101:
            subcommand = '1b'
        elif channel == 102:
            subcommand = '2a'
        elif channel == 103:
            subcommand = '2b'
        elif channel == 104:
            subcommand = '3a'
        elif channel == 105:
            subcommand = '3b'
        elif channel == 106:
            raise Exception("Not implemented Option")
        elif channel == 107:
            raise Exception("Not implemented Option")
        elif channel == 108:
            raise Exception("Not implemented Option")
        elif channel == 109:
            raise Exception("Not implemented Option")
        else:
            raise Exception("Not Valid option, only number 1 to 105")
        

        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        byte_array = bytearray.fromhex(cmd_send)
        ser.write(byte_array)

    except Exception as e:
        print(e)
        ser.close()


#S-Meter Level
def read_smeter_level(ser,preamble,trxadd,ctrladd,trailer):
    try:
        command = "15"
        subcommand = "02"
        data = ""
        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

        count = 0
        while count <= 1:
            for x in range(0, 6):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        
        
        num_value = int(info.lstrip("0"))
        print("Valor obtenido: " + str(num_value))
        if num_value < 129:
            resultado = "Menor S9"
        elif num_value == 129:
            resultado = "S9"
        elif num_value > 129 and num_value < 241:
            resultado = "Mayor a S9"
        elif num_value == 241:
            resultado = "S9+60"
        else:
            resultado = "Faaa que señalon papaaa"

        return resultado

    except Exception as e:
        print(e)
        ser.close()



########################################
# TESTING!!!!!!!!!!!!!!!!!!!!!!!!!!!!
########################################

#Memory Channel Content
#EN PROCESO
def get_memory_content(ser,preamble,trxadd,ctrladd,trailer):
    try:
        command = "1a"
        subcommand = "00"
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        ###############################
        print("#####################")
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)





        data = ""
        info = ""
        count = 0
        while count <= 1:
            for x in range(0, 5):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        resultado = info[8]+info[9]+info[6]+info[7]+"."+info[4]+info[5]+info[2]+"."+info[3]+info[0]+info[1]+"Hz"
        return resultado.lstrip("0")
    except Exception as e:
        print(e)
        ser.close()

#Power Level Meter
#EN PROCESO
def get_power_level_meter(ser,preamble,trxadd,ctrladd,trailer):
    try:
        print("Para ver el power level tiene que estar el equipo en TX")
        command = "15"
        subcommand = "11"
        data = ""
        info = ""
        cmd_send = preamble + preamble + trxadd + ctrladd + command + subcommand + trailer
        byte_array = bytearray.fromhex(cmd_send)
        s = ser.write(byte_array)

        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        ########
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)
        s = ser.read()
        print(s)







        count = 0
        while count <= 1:
            for x in range(0, 5):
                s = ser.read()
            s = ser.read()
            if s != bytearray.fromhex(trailer):
                while s != bytearray.fromhex(trailer):
                    data = s.hex()
                    info = info + data
                    s = ser.read()
            count += 1
        #ser.close()
        resultado = info[8]+info[9]+info[6]+info[7]+"."+info[4]+info[5]+info[2]+"."+info[3]+info[0]+info[1]+"Hz"
        return resultado.lstrip("0")
    except Exception as e:
        print(e)
        ser.close()



if __name__ == "__main__":
    main()
