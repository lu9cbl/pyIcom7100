#!/usr/bin/python3
#Extraído de https://www.mods.dk/download/1631812872_5901_IC-7100-CONTROL.py_


import sys, serial, time, threading, binascii, datetime, curses, os
from subprocess import run

command=bytearray()

#initialization and open the port

class SerialIcom(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self, daemon=True)
		self._stop = threading.Event()
		self.ser = serial.Serial(
			port="/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_IC-7100_03001616_A-if00-port0",
			baudrate=19200,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_TWO,
			bytesize=serial.EIGHTBITS,
			timeout=5,
			rtscts = False,    # disable hardware (RTS/CTS) flow control
			dsrdtr = False)
		self.ser.flushInput()
		self.ser.flushOutput()
		self.ser.close()

	def __enter__(self):
		try: 
			self.ser.open()
			return self
		except: # catch *all* exceptions
			e = sys.exc_info()[0]
			print( "error open serial port: " + str(e))
			exit()

	def __exit__(self, type, value, traceback):
		self.ser.close()

	def __del__(self):
		self.ser.close()
		self.ser.__del__()
		self._stop.set()

	def srx(self, textstring, check, command ):
		"""Listens via serial for headers, end of message, and returns payloads from IC-7100"""
		rxhead = bytearray((int(0xFE), int(0xFE),int(0x00), int(0x88)))
		cflag = True
		cnt = 0
		while cflag:
			if self.ser.inWaiting():
				sin = self.ser.read(size=1)
				if sin is not None:
					if ord(sin) == rxhead[cnt]:
						cnt += 1
						if cnt > 3:	# header received, on to payload!
							cflag = False
		cflag = True
		cnt = 0
		payload = bytearray()
		while cflag:
			if self.ser.inWaiting():
				sin = self.ser.read(size=1)
				if ord(sin) != int(0xFD):	# 0xFD signals end of message from icom
					payload.append(ord(sin))
					cnt += 1
					if cnt > 16:
						cflag = False
						print("Maximum payload exceeded")
				else:
					cflag = False
					if check:
#						print(binascii.hexlify(payload))
#						print(binascii.hexlify(command))
						if payload[1:] == command:
							print("Command " + textstring + " Checked")
						else:
							print("Error, command " + textstring + " not confirmed")
							exit()
					else:
						if ord(payload) != int(0xFB):
							print("Error " + textstring + " not FB")
							exit()
						else:
							print("Command " + textstring + " ", hex(command[-1]), " FB")
					return payload

	def stx(self, leader, command):
		"""Sends a command via serial to the ICOM IC-7100"""
		self.ser.flushOutput()
		self.ser.flushInput()
		txmsg=leader
		txmsg.extend(bytearray((int(0xFE), int(0xFE), int(0x88), int(0x00))))
		txmsg.extend(command)
		txmsg.append(int(0xFD))
		len=self.ser.write(txmsg)
		return len

	def flushsrx(self):
		self.ser.flushInput()

def cmdSetFreq(cli, f100MHz, f10MHz, fMHz, f100kHz, f10kHz, fkHz, f100Hz, f10Hz, fHz):
# Freq byte sequence:   10Hz|Hz  kHz|100Hz  100kHz|10kHz  10MHz|MHz  GHz|100MHz
	leader=bytearray()
	command=bytearray((int(0x00),))	# freq command code
	el = (f10Hz * 16) + fHz
	command.append(el)
	el = (fkHz * 16) + f100Hz
	command.append(el)
	el = (f100kHz * 16) + f10kHz
	command.append(el)
	el = (f10MHz * 16) + fMHz
	command.append(el)
	el = f100MHz					# no GHz needed
	command.append(el)
	ln = cli.stx(leader, command)
	ln = cli.stx(leader, bytearray((int(0x00),))) # freq command code; set freq does not produce confirm, so ask !
	pl = cli.srx("SetFreq", 1, command)

# 00:LSB 01:USB 02:AM 03:CW 04:RTTY 05:FM 06:WFM 07:CW-R 08:RTTY-R 17:DV
def cmdSetMode(cli, mode, fil):
	leader = bytearray()
	command = bytearray((int(0x06), mode, fil))
	ln = cli.stx(leader, command)
	pl = cli.srx("SetMode", 0, command)

def cmdTXmonitor(cli, state):
	leader = bytearray()
	command = bytearray((int(0x1A), int(0x05), int(0x00), int(0x01), state))	# TX Monitor ON | OFF
	ln = cli.stx(leader, command)
	pl = cli.srx("TXmonitor", 0, command)

def cmdSpeakerOut(cli,state):
	leader = bytearray()
	command = bytearray((int(0x1A), int(0x05), int(0x00), int(0x35), state))	# Speaker out ON | OFF
	ln = cli.stx(leader, command)
	pl = cli.srx("SpeakerOut", 0, command)

def cmdRXTX(cli,state):
	leader = bytearray()
	command = bytearray((int(0x1C), int(0x00), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("RXTX", 0, command)

def cmdOnOff(cli,state):
	leader = bytearray()
	if state:
		leader = bytearray([int(0xFE)] * 25 ) # 25 times switches set on @ 19200 baudrate
	command = bytearray((int(0x18), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("OnOff", 0, command)

def cmdSourceNonData(cli,state):
	leader = bytearray()
	command = bytearray((int(0x1A), int(0x05), int(0x00), int(0x90), state))
	ln = cli.stx(leader, command)
	pl = cli.srx("SourceNonData", 0, command)

def cmdUsbLevel(cli,state):
	perc = int(state)
	pld = int( 256 * perc / 100 )
	d1 = int( pld / 100 )
	rest = pld - ( d1 * 100 )
	d2 = int( rest / 10 )
	rest -= d2 * 10
	plsb = int( (d2 * 16) + rest ) 
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x00), int(0x89), d1 , plsb ))
	ln = cli.stx(leader, command)
	pl = cli.srx("UsbLevel", 0, command)

def cmdVFOMode(cli,state):
	leader = bytearray()
	command = bytearray((int(0x07), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("VFOMode", 0, command)

def cmdSplitDup(cli,state):
	leader = bytearray()
	command = bytearray((int(0x0F), state )) # 00:Split OFF , 01:Split ON , 10:Simplex , 11:DUP– , 12:DUP+
	ln = cli.stx(leader, command)
	pl = cli.srx("SplitDup", 0, command)

def cmdPreAmp(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x02), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("PreAmp", 0, command)

def cmdAGC(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x12), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("AGC", 0, command)

def cmdNB(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x22), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("NB", 0, command)

def cmdNR(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x40), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("NR", 0, command)

def cmdNotch(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x41), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("Notch", 0, command)

def cmdRepTone(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x42), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("RepTone", 0, command)

def cmdToneSquelch(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x43), state ))
	cli.stx(leader, command)
	ln = cli.stx(leader, command)
	pl = cli.srx("ToneSquelch", 1, command)

def cmdSpeechCompr(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x44), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("SpeechCompr", 0, command)

def cmdVOX(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x46), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("VOX", 0, command)

def cmdManualNotch(cli,state):
	leader = bytearray()
	command = bytearray((int(0x16), int(0x48), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("ManualNotch", 0, command)

def cmdDateTimeNow(cli):
	leader = bytearray()
	datenow = datetime.datetime.now().strftime("%Y%m%d")
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x20), int(datenow[0:2],16), int(datenow[2:4],16), int(datenow[4:6],16), int(datenow[6:8],16) ))	# Date
	ln = cli.stx(leader, command)
	pl = cli.srx("Date", 0, command)
	leader = bytearray()
	timenow = datetime.datetime.now().strftime("%H%M")
	command = bytearray((int(0x1A), int(0x05), int(0x01), int(0x21), int(timenow[0:2],16), int(timenow[2:4],16) ))	# Time
	ln = cli.stx(leader, command)
	pl = cli.srx("Time", 0, command)

def cmdRecord(cli,state):
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x70), state ))
	ln = cli.stx(leader, command)
	pl = cli.srx("Record", 0, command)

def cmdRecordSettings(cli):
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x67), int(0x01) )) # RX Only
	ln = cli.stx(leader, command)
	pl = cli.srx("RX Record only", 0, command)
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x68), int(0x01) )) # When squelch open only
	ln = cli.stx(leader, command)
	pl = cli.srx("Squelch Open Only", 0, command)
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x69), int(0x01) )) # Split Files On
	ln = cli.stx(leader, command)
	pl = cli.srx("Split Recordings over Files", 0, command)
	leader = bytearray()
	command = bytearray(( int(0x1A), int(0x05), int(0x01), int(0x70), int(0x01) )) # Auto PTT
	ln = cli.stx(leader, command)
	pl = cli.srx("Auto PTT", 0, command)

def cmdSkype(cli):
	stdscr = curses.initscr()
	curses.noecho()
	curses.cbreak()
	stdscr.keypad(True)
	tx = 0
	while 1:
		key=""
		stdscr.clear()
		stdscr.addstr("o(ver) | s(top)")
		key = stdscr.getch()
		stdscr.clear()
		stdscr.addstr("Detected key:")
		stdscr.addstr(str(key))
		if key == ord('s'):
			break
		elif key == ord('o'):
			if tx == 1:
				tx = 0
				cmdRXTX(cli,0)	
			else:
				tx = 1
				cmdRXTX(cli,1)
		else:
			stdscr.clear() 
			stdscr.addstr("o(ver) | s(top)")
	curses.nocbreak()
	stdscr.keypad(False)
	curses.echo()
	curses.endwin()

def main(argv):
	if len(argv):
		with SerialIcom() as cli:
			if argv[0] == "example":
				cmdOnOff(cli,1)				# Switch ON
				cmdRXTX(cli,0)				# RX
				cmdVFOMode(cli, 0)			# VFO A
				cmdSetMode(cli, 1, 2)		# USB Filter-2
				cmdTXmonitor(cli, 1)		# TX Monitor ON
				cmdSpeakerOut(cli, 1)		# Speaker Out ON
				cmdSourceNonData(cli, 0)	# Mic non-data source
				cmdSplitDup(cli,0)			# No Split
				cmdSplitDup(cli, int(0x10)) # Simplex
				cmdPreAmp(cli, 0)			# Pre Amp OFF
				cmdAGC(cli, 2)				# AGC Mid
				cmdNB(cli, 1)				# NB ON
				cmdNR(cli, 0)				# NR OFF
				cmdNotch(cli, 0)			# Notch OFF
				cmdRepTone(cli, 0)			# Rep Tone OFF
				cmdToneSquelch(cli, 0)		# Tone Squelch OFF
				cmdSpeechCompr(cli, 0)		# OFF
				cmdVOX(cli, 0)				# OFF
				cmdManualNotch(cli, 0)		# OFF
				cmdSetFreq(cli, 0, 1, 4, 2, 0, 0, 0, 0, 0) # 14.345MHz
				cmdDateTimeNow(cli)
			elif argv[0] == "txmonoff":
				cmdTXmonitor(cli, 0)		# TX Monitor OFF
			elif argv[0] == "rx":
				cmdRXTX(cli,0)				# RX
				cmdSpeakerOut(cli, 1)		# Speaker Out ON
			elif argv[0] == "tx":
				cmdSpeakerOut(cli, 0)		# Speaker Out OFF
				cmdRXTX(cli,1)				# TX
			elif argv[0] == "on":
				cmdOnOff(cli,1)				# Switch ON
			elif argv[0] == "off":
				cmdOnOff(cli,0)				# Switch OFF
			elif argv[0] == "mic":
				cmdSourceNonData(cli, 0)	# Mic non-data source
			elif argv[0] == "usb":
				cmdSourceNonData(cli, 3)	# USB non-data source
			elif argv[0] == "usblevel":
				cmdUsbLevel(cli, argv[1])	# USB modulation level %
			elif argv[0] == "playwav":
				cmdSourceNonData(cli, 3)	# USB non-data source
				cmdRXTX(cli,1)				# TX
				cmdUsbLevel(cli, 40)		# USB modulation level
				cmdTXmonitor(cli, 1)		# TX monitor on
#				cmdSpeakerOut(cli, 0)		# Speaker Out OFF
				apath = "</path to where audio files are/>"
				apath += argv[1]
				run([ "aplay", "--device=plughw:CARD=CODEC,DEV=0","-t", "wav", apath])
				cmdRXTX(cli,0)				# RX
#				cmdSpeakerOut(cli, 1)		# Speaker Out ON
				cmdUsbLevel(cli, 50)		# USB modulation level
				cmdTXmonitor(cli, 0)		# TX monitor off
				cmdSourceNonData(cli, 0)	# Mic non-data source
			elif argv[0] == "plaympg":
				cmdSourceNonData(cli, 3)	# USB non-data source
				cmdUsbLevel(cli, 40)		# USB modulation level
				cmdTXmonitor(cli, 1)		# TX monitor on
#				cmdSpeakerOut(cli, 0)		# Speaker Out OFF
				cmdRXTX(cli,1)				# TX
				apath = "</path to where audio files are/>"
				apath += argv[1]
				run([ "mplayer", "-ao" , "alsa:device=plughw=1.0" , apath ])
				cmdRXTX(cli,0)				# RX
#				cmdSpeakerOut(cli, 1)		# Speaker Out ON
				cmdUsbLevel(cli, 50)		# USB modulation level
				cmdSourceNonData(cli, 0)	# Mic non-data source
				cmdTXmonitor(cli, 0)		# TX monitor off
			elif argv[0] == "datetimenow":
				cmdDateTimeNow(cli)			# DateTimeNow
			elif argv[0] == "startrecord":
				cmdRecord(cli, 1)			# Record audio start
				cmdRXTX(cli, 1)
				cmdRXTX(cli, 0)
			elif argv[0] == "stoprecord":
				cmdRecord(cli, 0)			# Record audio start
			elif argv[0] == "recordsettings":
				cmdRecordSettings(cli)		# Record Settings
				cmdDateTimeNow(cli)
			elif argv[0] == "skype":
				cmdDateTimeNow(cli)
				cmdSourceNonData(cli, 3)	# USB non-data source
				cmdTXmonitor(cli, 1)		# TX Monitor ON
				cmdSpeakerOut(cli, 1)		# Speaker Out ON
				cmdSplitDup(cli,0)			# No Split
				cmdSplitDup(cli, int(0x10)) # Simplex
				cmdPreAmp(cli, 0)			# Pre Amp OFF
				cmdAGC(cli, 2)				# AGC Mid
				cmdNB(cli, 1)				# NB ON
				cmdNR(cli, 0)				# NR OFF
				cmdNotch(cli, 0)			# Notch OFF
				cmdRepTone(cli, 0)			# Rep Tone OFF
				cmdToneSquelch(cli, 0)		# Tone Squelch OFF
				cmdSpeechCompr(cli, 0)		# OFF
				cmdVOX(cli, 0)				# OFF
				cmdManualNotch(cli, 0)		# OFF
				cmdSkype(cli)				# Skype mode
				cmdSourceNonData(cli, 0)	# Mic non-data source
				cmdTXmonitor(cli, 0)		# TX Monitor OFF
	else:
		print("Give argument! : example | txmonoff | tx | rx | on | off | playwav <file name> | plaympg <file name> | mic | usb | usblevel <%> | datetimenow | startrecord | stoprecord | recordsettings ")

if __name__ == '__main__':
	main(sys.argv[1:])